#!/bin/python2.7
from google import search
from bs4 import BeautifulSoup
import sys
import urllib2
import socks
import socket
import os
import argparse
import time
import logging

def downloadUrlFile(url,AUTO_DL,outputFile):

	if not outputFile:
		file_name = url.split('/')[-1]
	else:
		file_name = outputFile

	localfile = os.getcwd() + "/" + file_name
	file_size_dl = 0
	req = urllib2.Request(url)
	answer="."

	try:
		if os.path.isfile(localfile):
			if not AUTO_DL:
				while not answer in ["Y","N"]:
					answer = raw_input("File "+ file_name + " gia' presente, riprendere il download? Y/N ").upper()

			if answer == "Y" or AUTO_DL:
				file_size_dl = os.path.getsize(localfile)
				req.headers['Range'] = 'bytes=%s-' % (file_size_dl)
				f = open(file_name, 'ab')
			else:
				f = open(file_name, 'wb')

		else:
			f = open(file_name, 'wb')
	except:
		logging.exception("Errore apertura file")
		quit()

	file_size = getUrlFileSize(url,"B")

	if file_size_dl >= file_size:
		print("File gia' scaricato")
	else:
		u = urllib2.urlopen(req)
		meta = u.info()
		block_size = 65536#8192

		print("Downloading: %s Bytes: %s" % (file_name, file_size))

		while True:
			time1 = time.time()
			buffer = u.read(block_size)
			if not buffer:
				break
			file_size_dl += len(buffer)
			f.write(buffer)
			time2 = time.time()
			status = r"%4d/%d MB [%3.2f%%]  %5d KiB/s" % (file_size_dl/1000000, file_size/1000000,file_size_dl * 100. / file_size, len(buffer)/(1000*(time2-time1)))
			print(status)

	f.close()

def getUrlFileSize(url,format):
	div = {'MB': 1000000, 'KB': 1000, 'B': 1}

	try:
		u = urllib2.urlopen(url)
		meta = u.info()
		return int(meta.getheaders("Content-Length")[0])/div[format]
	except:
		return 0

def getGoogleSeachPages(subject,nR):
	urls = []
	print("Ricerca di < "+ subject + " > in corso")

	for url in search('intitle:"index of" '+ subject, num=nR, stop=5):
		urls.append(url)

	return urls

def setSocketProxy():
	try:
		socks.setdefaultproxy(proxy_type=socks.PROXY_TYPE_SOCKS5, addr="127.0.0.1", port=9050)
		socket.socket = socks.socksocket
	except:
		logging.exception("Errore configurazione tor proxy")
		quit()

def getVideosLinks(urls,subject):
	videoFormat = ["mkv","mp4","wmv"]
	videoLinks = []
	videoSizes = []

	print("Analisi video...")
	print("Download pagine...")
	for url in urls:
		try:
			logging.info("Download pagina")
			response = urllib2.urlopen(url)
			htmlPage = response.read()
		except:
			logging.error("Errore download pagina")
			continue

		logging.debug("Analisi pagina " + url)
		soup = BeautifulSoup(htmlPage, 'html.parser')

		for links in soup.find_all('a'):
			try:
				link = url + links.get('href')
				logging.debug("Analisi link " + link)

				if link[len(link)-3:] in videoFormat and all(word.upper() in link.upper() for word in  subject.split()):
					logging.debug("Link video " + link + " accettato")

					size = getUrlFileSize(link,"MB")
					if size > 10:
						videoLinks.append(link)
						videoSizes.append(size)
			except:
				pass

	print("Risultati generati")
	return videoLinks, videoSizes

def searchMenu(urls,subject,AUTO_DL,nR,outputFile):
	videoLinks, videoSizes = getVideosLinks(urls,subject)
	
	if not AUTO_DL:
		print("0) Esci")
		for n in range(1,len(videoLinks)+1):
			print(str(n)+") " + videoLinks[n-1] + " " + str(videoSizes[n-1]) + " MB")

		answer=-1
		while not int(answer) in range(0,len(videoLinks)+1):
			try:
				answer = int(raw_input("File da scaricare: "))
			except:
				pass

		if answer == 0:
			quit()
		else:
			downloadUrlFile(videoLinks[int(answer)-1],AUTO_DL,outputFile)
	else:
		downloadUrlFile(videoLinks[0],AUTO_DL,outputFile)

def main():
	loggingLevels = {1:logging.WARNING,2:logging.INFO,3:logging.DEBUG}

	parser = argparse.ArgumentParser(description="Script in python per scaricare file video tramite tor sfruttando le funzioni di ricerca avanzate di google.")
	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument("-s","--search",action="store", dest="search", help="Stringa di ricerca", default="")
	group.add_argument("-l","--link", action="store",dest="link", help="Download diretto del link",default="")
	parser.add_argument('-n', action="store", dest="numero_ricerche", type=int, default=5,help="Numero di risultati cercati su google. Default: 5")
	parser.add_argument("-t","--no-tor", action="store_false", default=True, dest="notor", help="Non usare tor per il download (non sicuro)")
	parser.add_argument("-a","--auto-dl", action="store_true", default=False, dest="autodl", help="Scarica automaticamente il primo risultato senza chiedere")
	parser.add_argument("-o","--output-file", action="store",default="", dest="outputFile", help="Nome del file di output")
	parser.add_argument("-v","--verbose", action="store",default=1, type=int, dest="verboseLevel", help="Output avanzato. Warning 1, Info 2, Debug 3. Default: 1")

	arg = parser.parse_args()

	try:
		logging.basicConfig(format='%(levelname)s:%(message)s', level=loggingLevels[arg.verboseLevel])
	except:
		logging.basicConfig(format='%(levelname)s:%(message)s', level=loggingLevels[1])
		logging.error("Valore verbose non valido")

	if arg.autodl:
		print("Auto download")

	if arg.link:
		if arg.notor:
			print("Configurazione tor proxy")
			setSocketProxy()
		else:
			logging.warning('Tor proxy non configurato, download non sicuro')

		downloadUrlFile(arg.link,arg.autodl,arg.outputFile)
	else:
		urls = getGoogleSeachPages(arg.search,arg.numero_ricerche)

		if arg.notor:
			print("Configurazione tor proxy")
			setSocketProxy()
		else:
			logging.warning('Tor proxy non configurato, download non sicuro')

		searchMenu(urls,arg.search,arg.autodl,arg.numero_ricerche,arg.outputFile)

if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		print('\nUscita')
		quit()