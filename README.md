Script in python per scaricare file video tramite tor sfruttando le funzioni di ricerca avanzate di google.


Dipendenze:

* tor: `sudo apt-get install tor` `sudo systemctl start tor`

* pysocks: `sudo pip2.7 install pysocks`

* BeautifulSoup4: `sudo pip2.7 install beautifulsoup4`

* google: `sudo pip2.7 install google`
 

Utilizzo: 

`pyTor.py [-h] (-s SEARCH | -l LINK) [-n NUMERO_RICERCHE] [-t] [-a] [-o OUTPUTFILE]` 

Opzioni:

`-s SEARCH, --search STRINGA` cosa cercare

`-l LINK` link da scaricare via tor senza effettuare ricerche

`-h, --help` mostra questa pagina
    
`-a, --autodl` scarica automaticamente il primo risultato generato
    
`-t, --no-tor` non usare tor per il download (non sicuro)

`-n NUMERO_RICERCHE` Numero di risultati cercati su google. Default: 5

`-o OUTPUTFILE, --output-file OUTPUTFILE` Nome del file di output

`-v VERBOSELEVEL, --verbose VERBOSELEVEL` Output avanzato. Warning 1, Info 2, Debug 3. Default: 1

Per funzionare bisogna utilizzre l'opione -s o l'opzione -l
    

Esempi:  
`./pyTor.py -s "stringa di ricerca" -n 3`

`./pyTor.py -l "link.mkv" -o "file.mkv"`

